from datetime import datetime, timedelta
import dateutil.parser
from pprint import pprint

VERBOSE = False


def log(message, verbose):
    if verbose:
        pprint(message)


def date_last_weekend_shift(weeks=1):
    today = datetime.utcnow()
    first_date = today - timedelta(days=today.weekday() + 1,
                                   hours=today.hour,
                                   minutes=today.minute,
                                   seconds=today.second,
                                   microseconds=today.microsecond) + timedelta(days=weeks * 7)
    second_date = first_date + timedelta(days=1)
    return [second_date.isoformat(), first_date.isoformat()]


def iso_to_datetime(strptime):
    return dateutil.parser.parse(strptime)
