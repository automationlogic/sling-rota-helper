## Rota helper tool
### Pre-requisites
Python version: 3

Modules required: `autoyaml`, `python-dateutil`, `requests`, `ortools`

Copy `team_data_template.py` to `team_data.py`

### Add requests and holidays
Adjustable variables are found in `team_data.py` - requests, holidays and week to calculate rota for. By default it will create a rota for the week after current (always beginning from Monday).

### Running the script
`python3 create_rota`
