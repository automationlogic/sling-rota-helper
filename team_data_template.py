#!/usr/bin/env python3

################################################################################
# Variables that are mostly fixed
################################################################################
# Firstnames according to sling
SENIOR = ["Matt", "Martyn", "Prem", "Richard", "Julian"]
JUNIOR = ["Amrit", "Danish", "Lily", "Mohammad", "Vincent", "Catherine",
          "Nabeel", "Rahmat"]

ENGINEERS = SENIOR + JUNIOR


# Shift types, beginning with OFF
SHIFTS = ['OFF', 'ON-SITE', 'EARLY', 'DAY', 'EVENING']
WEEKDAYS = ["MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"]

# Starting hour, corresponding to position of shift type in list SHIFTS (no OFF)
SHIFT_TIMES = {"9": 1, "0": 2, "8": 3, "16": 4}

################################################################################
# Variables to change on a weekly basis
################################################################################
WEEKS_IN_FUTURE = 1  # Calculate rota for next week

# (Employee, [List of Days])
HOLIDAYS = [
    ("Matt", ["MON", "TUE", "FRI", "SAT", "SUN"]),
    ("Mohammad", ["MON"]),
    ("Rahmat", ["THU", "FRI", "SAT", "SUN"])
]

# (Employee, Shift, Day)
REQUESTS = [
    ("Rahmat", "EARLY", "WED"),
    ("Julian", "ON-SITE", "MON"),
    ("Julian", "ON-SITE", "TUE"),
    ("Julian", "ON-SITE", "WED"),
    ("Julian", "ON-SITE", "THU"),
    ("Julian", "ON-SITE", "FRI"),
]
