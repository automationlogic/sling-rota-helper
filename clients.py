import requests
from utils import log
OUR_TEAM = []


class User(object):
    def __init__(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name
        self.name = '{} {}'.format(self.first_name, self.last_name)
        OUR_TEAM.append(self.name)


class Client(object):
    custom_headers = {}
    auth_header = ''

    def __init__(self, verbose=False):
        self.verbose = verbose

    def get(self, path, **kwargs):
        query_string = ''
        self.custom_headers['Authorization'] = self.auth_header
        if kwargs:
            query_string = "?"
            for key, value in kwargs.items():
                query_string += '{}={}&'.format(key, value)
            query_string = query_string[:-1:]
        log('GET ' + self.url + path + query_string, self.verbose)
        req = requests.get(self.url + path + query_string, headers=self.custom_headers)
        log(req, self.verbose)
        return req

    def post(self, path, data):
        self.custom_headers['Authorization'] = self.auth_header
        log('POST ' + self.url + path, self.verbose)
        log(data, self.verbose)
        req = requests.post(self.url + path, headers=self.custom_headers, json=data)
        log(req, self.verbose)
        return req

    def put(self, path, data):
        self.custom_headers['Authorization'] = self.auth_header
        log('PUT ' + self.url + path, self.verbose)
        log(data, self.verbose)
        req = requests.put(self.url + path, headers=self.custom_headers, json=data)
        log(req, self.verbose)
        return req


class Sling(Client):
    def __init__(self, sling_email, sling_password, verbose, *args, **kwargs):
        self.username = sling_email
        self.password = sling_password
        self.verbose = verbose
        self.address = 'https://api.sling.is'
        self.api_version = 'v1'
        self.url = '/'.join([self.address, self.api_version])
        self.login()
        self.map_shifts()
        self.our_team = {u['id']: self.create_user(
            u) for u in self.get('/users').json() if u.get('active')}

    def login(self):
        data = {'email': self.username, 'password': self.password}
        log('Sending login request for user {}'.format(self.username), self.verbose)
        req = requests.post(self.url + '/account/login', json=data)
        self.auth_header = req.headers['Authorization']
        self.session = req.json()
        self.organization = self.session['org']['id']
        self.user = self.session['user']['id']
        log('auth_header is {}'.format(self.auth_header), self.verbose)

    def create_user(self, user_dict):
        return user_dict['name']

    def map_shifts(self):
        self.id_to_name = {}
        for group in self.session['user']['groups']:
            if group.get('type') == 'location':
                self.id_to_name[group['id']] = group['name']
