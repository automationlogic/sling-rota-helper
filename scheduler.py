#!/usr/bin/env python3
from __future__ import print_function
from ortools.sat.python import cp_model
from google.protobuf import text_format
import random
import json
import team_data as team
from utils import date_last_weekend_shift, iso_to_datetime
from autoyaml import load_config, write_config
from clients import Sling
from getpass import getpass


def negated_bounded_span(works, start, length):
    sequence = []
    # Left border (start of works, or works[start - 1])
    if start > 0:
        sequence.append(works[start - 1])
    for i in range(length):
        sequence.append(works[start + i].Not())
    # Right border (end of works or works[start + length])
    if start + length < len(works):
        sequence.append(works[start + length])
    return sequence


def add_soft_sequence_constraint(model, works, hard_min, soft_min, min_cost,
                                 soft_max, hard_max, max_cost, prefix):

    cost_literals = []
    cost_coefficients = []

    # Forbid sequences that are too short.
    for length in range(1, hard_min):
        for start in range(len(works) - length + 1):
            model.AddBoolOr(negated_bounded_span(works, start, length))

    # Penalize sequences that are below the soft limit.
    if min_cost > 0:
        for length in range(hard_min, soft_min):
            for start in range(len(works) - length + 1):
                span = negated_bounded_span(works, start, length)
                name = ': under_span(start=%i, length=%i)' % (start, length)
                lit = model.NewBoolVar(prefix + name)
                span.append(lit)
                model.AddBoolOr(span)
                cost_literals.append(lit)
                # We filter exactly the sequence with a short length.
                # The penalty is proportional to the delta with soft_min.
                cost_coefficients.append(min_cost * (soft_min - length))

    # Penalize sequences that are above the soft limit.
    if max_cost > 0:
        for length in range(soft_max + 1, hard_max + 1):
            for start in range(len(works) - length + 1):
                span = negated_bounded_span(works, start, length)
                name = ': over_span(start=%i, length=%i)' % (start, length)
                lit = model.NewBoolVar(prefix + name)
                span.append(lit)
                model.AddBoolOr(span)
                cost_literals.append(lit)
                # Cost paid is max_cost * excess length.
                cost_coefficients.append(max_cost * (length - soft_max))

    # Just forbid any sequence of true variables with length hard_max + 1
    for start in range(len(works) - hard_max):
        model.AddBoolOr(
            [works[i].Not() for i in range(start, start + hard_max + 1)])
    return cost_literals, cost_coefficients


def add_soft_sum_constraint(model, works, hard_min, soft_min, min_cost,
                            soft_max, hard_max, max_cost, prefix):

    cost_variables = []
    cost_coefficients = []
    sum_var = model.NewIntVar(hard_min, hard_max, '')
    # This adds the hard constraints on the sum.
    model.Add(sum_var == sum(works))

    # Penalize sums below the soft_min target.
    if soft_min > hard_min and min_cost > 0:
        delta = model.NewIntVar(-len(works), len(works), '')
        model.Add(delta == soft_min - sum_var)
        # TODO(user): Compare efficiency with only excess >= soft_min - sum_var.
        excess = model.NewIntVar(0, 7, prefix + ': under_sum')
        model.AddMaxEquality(excess, [delta, 0])
        cost_variables.append(excess)
        cost_coefficients.append(min_cost)

    # Penalize sums above the soft_max target.
    if soft_max < hard_max and max_cost > 0:
        delta = model.NewIntVar(-7, 7, '')
        model.Add(delta == sum_var - soft_max)
        excess = model.NewIntVar(0, 7, prefix + ': over_sum')
        model.AddMaxEquality(excess, [delta, 0])
        cost_variables.append(excess)
        cost_coefficients.append(max_cost)

    return cost_variables, cost_coefficients


def solve_shift_scheduling(params, output_proto):
    # Data
    num_employees = len(team.ENGINEERS)
    num_weeks = 1
    shifts = team.SHIFTS
    requests = []
    fixed_assignments = []

    # Account for previous shifts
    print('Loading config file')
    CONFIG = load_config('rota_helper')  # Load config using autoyaml
    DOCUMENTATION = ''
    # If the config hasn't been created then feed it in from user input
    if not CONFIG:
        print('Creating new Config file')
        for key in ['sling_email', 'sling_password']:
            if 'password' in key or 'api_token' in key:
                CONFIG[key] = getpass('{}?'.format(key))
            else:
                CONFIG[key] = input('{}?'.format(key))
        CONFIG['verbose'] = False
        write_config(CONFIG, 'rota_helper')
    VERBOSE = CONFIG.get('verbose')
    # Create clients from loaded config
    try:
        sling = Sling(**CONFIG)
    except Exception as e:
        print('Error message: {}'.format(e))
        print('Failed to create clients, check credentials')
        os.remove(os.path.expanduser('.pagerslingy'))
        exit(1)
    raw_shifts = sling.get('/calendar/{}/users/{}'.format(sling.organization, sling.user), dates='{}/{}'
                           .format(*date_last_weekend_shift(team.WEEKS_IN_FUTURE)))
    prevshifts = [s for s in json.loads(raw_shifts._content) if s['type'] == "shift"]
    prevweek = []
    for i in range(len(prevshifts)):
        prevweek.append((team.ENGINEERS.index(sling.our_team[prevshifts[i]['user']['id']]),
                        team.SHIFT_TIMES[str(iso_to_datetime(prevshifts[i]['dtstart']).hour)],
                        iso_to_datetime(prevshifts[i]['dtstart']).weekday()))

    last_weekend = [e[0] for e in [i for i in prevweek if i[2] == 6]]  # engineers working last sunday
    last_shift = [i for i in prevweek if i[1] == 4 and i[2] == 6][0][0]  # engineer on sunday evening shift
    # Engineers who could work this weekend
    weekend = random.sample(
        [x for x in list(range(team.ENGINEERS.index(team.JUNIOR[0]), len(team.ENGINEERS))) if
            x not in last_weekend and
            x not in
            [team.ENGINEERS.index(h[0]) for h in team.HOLIDAYS if any(d in "SAT, SUN" for d in h[1])]],
        3)

    # Calculate the normal/weekend split & set appropriate rest days.
    week = [i for i in list(range(0, num_employees)) if i not in weekend]  # these will have thurs/fri shifts
    for e in weekend:
        fixed_assignments.append((e, 0, 3))
        fixed_assignments.append((e, 0, 4))
    for e in week:
        fixed_assignments.append((e, 0, 5))
        fixed_assignments.append((e, 0, 6))

    # Applying holidays
    for h in team.HOLIDAYS:
        for d in h[1]:
            fixed_assignments.append(
                (team.ENGINEERS.index(h[0]), 0, team.WEEKDAYS.index(d))
            )
    # Request: (employee, shift, day, weight)
    # A negative weight indicates that the employee desires this assignment.
    for r in team.REQUESTS:
        requests.append(
            (team.ENGINEERS.index(r[0]), team.SHIFTS.index(r[1]), team.WEEKDAYS.index(r[2]), -10)
        )

    requests.append((last_shift, 2, 0, 0))  # Cannot go from sunday night to monday morning
    for senior in range(0, len(team.SENIOR)):  # Seniors have more on-site shifts.
        for day in range(0, 5):  # MON-FRI
            requests.append((senior, 1, day, -2))
    for e in weekend:  # Weekend workers are on-site during week.
        for day in range(0, 3):  # MON-WED
            requests.append((e, 1, day, -2))

    # Shift constraints on continuous sequence:
    #    (shift, hard_min, soft_min, min_penalty, soft_max, hard_max, max_penalty)
    shift_constraints = [
        # Two consecutive days of rest.
        (0, 1, 2, 10, 2, 7, 10),
        # Two to four consecutive days of on-site.
        (1, 0, 2, 7, 4, 5, 7),
    ]

    # Weekly sum constraints on shifts days:
    #     (shift, hard_min, soft_min, min_penalty, soft_max, hard_max, max_penalty)
    weekly_sum_constraints = [
        # two rest days per week.
        (0, 0, 2, 10, 2, 7, 10),
    ]

    # Penalized transitions:
    #     (previous_shift, next_shift, penalty (0 means forbidden))
    # OFF 0, ON-SITE 1, EARLY 2, DAY 3, NIGHT 4
    penalized_transitions = [
        (1, 4, 0),
        (3, 4, 0),
        (4, 2, 0),
        (2, 3, 0),
        (2, 1, 0),
        (4, 3, 4),
        (4, 1, 4),
    ]

    # daily demands for work shifts (on-site, early, day, evening)
    weekly_cover_demands = [
        (1, 1, 1, 1),  # Monday
        (1, 1, 1, 1),  # Tuesday
        (1, 1, 1, 1),  # Wednesday
        (1, 1, 1, 1),  # Thursday
        (1, 1, 1, 1),  # Friday
        (0, 1, 1, 1),  # Saturday
        (0, 1, 1, 1),  # Sunday
    ]

    # Penalty for exceeding the cover constraint per shift type.
    excess_cover_penalties = (-2, 7, 7, 7)

    num_days = num_weeks * 7
    num_shifts = len(shifts)

    model = cp_model.CpModel()

    work = {}
    for e in range(num_employees):
        for s in range(num_shifts):
            for d in range(num_days):
                work[e, s, d] = model.NewBoolVar('work%i_%i_%i' % (e, s, d))

    # Linear terms of the objective in a minimization context.
    obj_int_vars = []
    obj_int_coeffs = []
    obj_bool_vars = []
    obj_bool_coeffs = []

    # Exactly one shift per day.
    for e in range(num_employees):
        for d in range(num_days):
            model.Add(sum(work[e, s, d] for s in range(num_shifts)) == 1)

    # Fixed assignments.
    for e, s, d in fixed_assignments:
        model.Add(work[e, s, d] == 1)

    # Employee requests
    for e, s, d, w in requests:
        obj_bool_vars.append(work[e, s, d])
        obj_bool_coeffs.append(w)

    # Shift constraints
    for ct in shift_constraints:
        shift, hard_min, soft_min, min_cost, soft_max, hard_max, max_cost = ct
        for e in range(num_employees):
            works = [work[e, shift, d] for d in range(num_days)]
            variables, coeffs = add_soft_sequence_constraint(
                model, works, hard_min, soft_min, min_cost, soft_max, hard_max,
                max_cost, 'shift_constraint(employee %i, shift %i)' % (e,
                                                                       shift))
            obj_bool_vars.extend(variables)
            obj_bool_coeffs.extend(coeffs)

    # Weekly sum constraints
    for ct in weekly_sum_constraints:
        shift, hard_min, soft_min, min_cost, soft_max, hard_max, max_cost = ct
        for e in range(num_employees):
            for w in range(num_weeks):
                works = [work[e, shift, d + w * 7] for d in range(7)]
                variables, coeffs = add_soft_sum_constraint(
                    model, works, hard_min, soft_min, min_cost, soft_max,
                    hard_max, max_cost,
                    'weekly_sum_constraint(employee %i, shift %i, week %i)' %
                    (e, shift, w))
                obj_int_vars.extend(variables)
                obj_int_coeffs.extend(coeffs)

    # Penalized transitions
    for previous_shift, next_shift, cost in penalized_transitions:
        for e in range(num_employees):
            for d in range(num_days - 1):
                transition = [
                    work[e, previous_shift, d].Not(),
                    work[e, next_shift, d + 1].Not()
                ]
                if cost == 0:
                    model.AddBoolOr(transition)
                else:
                    trans_var = model.NewBoolVar(
                        'transition (employee=%i, day=%i)' % (e, d))
                    transition.append(trans_var)
                    model.AddBoolOr(transition)
                    obj_bool_vars.append(trans_var)
                    obj_bool_coeffs.append(cost)

    # Cover constraints
    for s in range(1, num_shifts):
        for w in range(num_weeks):
            for d in range(7):
                works = [work[e, s, w * 7 + d] for e in range(num_employees)]
                # Ignore Off shift.
                min_demand = weekly_cover_demands[d][s - 1]
                worked = model.NewIntVar(min_demand, num_employees, '')
                model.Add(worked == sum(works))
                over_penalty = excess_cover_penalties[s - 1]
                if over_penalty > 0:
                    name = 'excess_demand(shift=%i, week=%i, day=%i)' % (s, w,
                                                                         d)
                    excess = model.NewIntVar(0, num_employees - min_demand,
                                             name)
                    model.Add(excess == worked - min_demand)
                    obj_int_vars.append(excess)
                    obj_int_coeffs.append(over_penalty)

    # Objective
    model.Minimize(
        sum(obj_bool_vars[i] * obj_bool_coeffs[i]
            for i in range(len(obj_bool_vars)))
        + sum(obj_int_vars[i] * obj_int_coeffs[i]
              for i in range(len(obj_int_vars))))

    if output_proto:
        print('Writing proto to %s' % output_proto)
        with open(output_proto, 'w') as text_file:
            text_file.write(str(model))

    # Solve the model.
    solver = cp_model.CpSolver()
    solver.parameters.num_search_workers = 8
    if params:
        text_format.Merge(params, solver.parameters)
    solution_printer = cp_model.ObjectiveSolutionPrinter()
    status = solver.SolveWithSolutionCallback(model, solution_printer)

    # Print solution.
    if status == cp_model.OPTIMAL or status == cp_model.FEASIBLE:
        print()
        header = '          '
        for w in range(num_weeks):
            header += 'MONDAY TUESDAY WEDNESDAY THURSDAY FRIDAY SATURDAY SUNDAY'
        print(header)
        for e in range(num_employees):
            schedule = ''
            for d in range(num_days):
                for s in range(num_shifts):
                    if solver.BooleanValue(work[e, s, d]):
                        schedule += "{:<7}".format(shifts[s]) + ' '
            print('%s: %s' % ("{:<9}".format(team.ENGINEERS[e]), schedule))
        # print()
        # print('Penalties:')
        # for i, var in enumerate(obj_bool_vars):
        #     if solver.BooleanValue(var):
        #         penalty = obj_bool_coeffs[i]
        #         if penalty > 0:
        #             print('  %s violated, penalty=%i' % (var.Name(), penalty))
        #         else:
        #             print('  %s fulfilled, gain=%i' % (var.Name(), -penalty))
        #
        # for i, var in enumerate(obj_int_vars):
        #     if solver.Value(var) > 0:
        #         print('  %s violated by %i, linear penalty=%i' %
        #               (var.Name(), solver.Value(var), obj_int_coeffs[i]))

    print()
    print(solver.ResponseStats())
